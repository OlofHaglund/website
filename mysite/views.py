from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.contrib import messages
from django.core.mail import send_mail, BadHeaderError

from .forms import Contact
#from .conf import settings

def index(request):
    form = Contact()
    return render(request, 'mysite/texts/about.html', {'form': form})

def contact(request):
    if request.method == 'POST':
        form = Contact(request.POST)

        if form.is_valid():
            subject = 'Message from: ' + request.POST['name'] + '<' + request.POST['mail'] + '>'
            message = request.POST['message'] + '\r\n\r\n' + 'Message from: ' + request.POST['name'] + '<' + request.POST['mail'] + '>'

            result = send_mail(subject, message, 'olof@olofhaglund.name', ['olof@olofhaglund.name']);
            if result == 0:
                messages.error(request, "Error sending the mail.<br /><br />You can also reach me on <a href=mailto:olof@olofhaglund.name>olof@olofhaglund.name</a>")
            else:
                messages.success(request, "Message is now sent")

    else:
        form = Contact()

    return render(request, 'mysite/oneSection/contact.html', {'form': form})
