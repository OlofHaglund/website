from django import forms

class Contact(forms.Form):
    name = forms.CharField(label='Name')
    mail = forms.EmailField(label='Email', required=True)
    message = forms.CharField(label='Message', widget=forms.Textarea, required=True)
